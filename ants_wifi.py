import esp32
import json
import machine
import network
import urequests
import time

from machine import Pin, PWM

CONFIG_PATH = "./config.json"

WIFI_TIMEOUT = 5

def load_config():
    with open(CONFIG_PATH, "r") as config_hdl:
        obj = json.load(config_hdl)
        return obj

def load_rinse_params(host, path, port):
    print("getting configuration from {}".format(host))
    response = urequests.get("http://%s:%s%s" % (host, port, path))
    ret = json.loads(response.text)
    return ret

def send_log_entry(log_entry, host, path, port):
    print("sending log to {}".format(host))
    urequests.post("http://%s:%s%s" % (host, port, path), headers = {'content-type': 'application/json'}, data = log_entry)

def rinse(AIN1, STBY, seconds):
    AIN1.on()
    STBY.on()
    time.sleep(seconds)
    AIN1.off()
    STBY.off()

def run():
    config = load_config()

    station = network.WLAN(network.STA_IF)
    station.active(True)
    station.connect(config["wifi_ssid"], config["wifi_password"])

    try_count = 0
    while not station.isconnected():
        time.sleep(1)
        try_count += 1
        print("try connecting ... {}".format(try_count))
        if try_count > WIFI_TIMEOUT:
            exit(1)

    rinse_params = load_rinse_params(config["server"], config["config_path"], config["port"])

    PWM_A = PWM(Pin(2))
    PWM_A.freq(rinse_params["freq"])
    PWM_A.duty(rinse_params["duty"])
    AIN1 = Pin(3,Pin.OUT)
    AIN2 = Pin(4,Pin.OUT)
    STBY = Pin(5, Pin.OUT)

    STBY.off()
    AIN1.off()
    AIN2.off()

    if machine.reset_cause() == machine.DEEPSLEEP_RESET:
        rinse(AIN1, STBY, rinse_params["rinse_time"])
    else:
        # load a tube by water when first run
        rinse(AIN1, STBY, rinse_params["load_rinse"])

    rinse_period_h = rinse_params["rinse_period"] / 1000 / 60 / 60
    if rinse_params["stop_sleep"] == False:
        message = 'rinsing for {}s, rinse period={}h'.format(rinse_params["rinse_time"], rinse_period_h)
        log_entry = json.dumps({'message': message})
        send_log_entry(log_entry, config["server"], config["log_path"], config["port"])
        machine.deepsleep(rinse_params["rinse_period"])
    else:
        message = 'rinsing for {}s, rinse period={}h, serving stopped'.format(rinse_params["rinse_time"], rinse_period_h)
        log_entry = json.dumps({'message': message})
        send_log_entry(log_entry, config["server"], config["log_path"], config["port"])

run()

