#!/bin/env python3
import json

from flask import Flask, request, jsonify

app = Flask(__name__)

CONFIG_PATH = "./ant_config.json"


@app.route("/ant_config")
def ant_config():
    config = load_config()
    return jsonify(config)


@app.route("/log", methods=['GET', 'POST'])
def log_entry():
    content = request.get_json()
    message = ""
    for key in content:
        value = content[key]
        entry = f"{key}: {value}"
        message += entry + ", "
    print(message[:-2])
    return jsonify({"OK": 200})


def load_config():
    with open(CONFIG_PATH, "r") as config_hdl:
        obj = json.load(config_hdl)
        return obj
    return None


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
