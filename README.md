# Ant Rinser w/ WiFi
## Description
This code is for ESP32C3 project for automatic ant rinsing. It loads configuration parameters from a simple web server on Flask, rinses formicarium, sends log entry back and sleep for the next rinse session.
The deep sleep is used to lower power consumption to minimum. The back side of the approach - you don't have access to the board until it wake up.
## Deployment
Connect TB6612FNG PWM driver to ESP32C3 board:
| TB6612FNG pins | ESP32C3 pins |
| -------------- | ------------ |
| PWMA           | GPIO2        |
| AIN1           | GPIO3        |
| AIN2           | GPIO4        |
| STBY           | GPIO5        |
| GND            | GND          |

Connect water pump to the A1 and A2 pins of the TB6612FNG board.

Assuming the ESP32C3 board is flashed by MicroPython image.
 - edit config.json file to replace the parameters:
   - server - put your server host or IP where the web server is to be deployed
   - wifi_ssid - put your home WiFi access point SSID
   - wifi_password - put your home WiFi access point password
 - copy config.json and ants_wifi.py to the ESP32C3 board memory, add "import ants_wifi" to the boot.py file
 - copy ant_config.json, ants.py, and requirements.txt to a web server host, install modules from requirements.txt
 - run web server, switch off/on the ESP32C3 board

 Now you can control the rinse period and other parameters through the ant_config.json file. Enjoy!

## Parameters
- rinse_period: rinsing period (milliseconds)
- rinse_time: time the water pump is on (seconds)
- load_rinse: time the water pump is on when first run to load the water tube (seconds)
- stop_sleep: put true here to stop processing and avoid sleeping the next rinse session (true|false)
- freq: PWM channel frequency (Hz)
- duty: PWM duty level (0..1023)

## Ingridients
Seeed Studio ESP32C3 w/WiFi antenna


![Seeed Studio ESP32C3](images/esp32c3.jpg)

TB6612FNG PWM driver for mini pump


![PWM driver for mini pump](images/driver.jpg)

Mini water pump (spare part for vaccum cleaner)


![Mini water pump](images/water_pump.jpg)
## TODO
  - Add power check and signal for low power
  - Add error handling
  - Add WebUI for changing parameters
